import {
  Controller,
  Get,
  // Post,
  // Body,
  // Patch,
  // Param,
  // Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';
// import { CreateReportDto } from './dto/create-report.dto';
// import { UpdateReportDto } from './dto/update-report.dto';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('/menu')
  getMenu() {
    return this.reportsService.getMenu();
  }

  @Get('/incomes')
  getIncomes(
    @Query()
    query: {
      startMonth: string;
      endMonth: string;
      year: string;
    },
  ) {
    return this.reportsService.getIncomesBy(
      parseInt(query.startMonth),
      parseInt(query.endMonth),
      query.year,
    );
  }

  @Get('/expenses')
  getExpenses(
    @Query()
    query: {
      startMonth: string;
      endMonth: string;
      year: string;
    },
  ) {
    return this.reportsService.getExpensesBy(
      parseInt(query.startMonth),
      parseInt(query.endMonth),
      query.year,
    );
  }

  @Get('nearly')
  getNearlyOut() {
    return this.reportsService.getNearlyOut();
  }

  @Get('/popfood')
  getPopFood() {
    return this.reportsService.getPopFood();
  }

  @Get('year')
  getYear() {
    return this.reportsService.getYearMaterials();
  }

  @Get('/expensematerials')
  getBuyMaterials(
    @Query()
    query: {
      startMonth: string;
      endMonth: string;
      year: string;
    },
  ) {
    return this.reportsService.getExpensesMaterialsBy(
      parseInt(query.startMonth),
      parseInt(query.endMonth),
      query.year,
    );
  }
  @Get('/sarary')
  getSarary(
    @Query()
    query: {
      startMonth: string;
      endMonth: string;
      year: string;
    },
  ) {
    return this.reportsService.getSalaryEsBy(
      parseInt(query.startMonth),
      parseInt(query.endMonth),
      query.year,
    );
  }
  @Get('/popdessert')
  getPopDessert() {
    return this.reportsService.getPopDessert();
  }
  @Get('/popbeverage')
  getPopBeverage() {
    return this.reportsService.getPopBeverage();
  }
  // @Post()
  // create(@Body() createReportDto: CreateReportDto) {
  //   return this.reportsService.create(createReportDto);
  // }

  // @Get()
  // findAll() {
  //   return this.reportsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.reportsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateReportDto: UpdateReportDto) {
  //   return this.reportsService.update(+id, updateReportDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.reportsService.remove(+id);
  // }
}
