import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { SalaryDetail } from './salary-detail';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  date_start: Date;
  @Column()
  date_end: Date;
  @Column()
  date_salary: Date;
  @Column({ type: 'float' })
  total: number;

  @OneToMany(
    () => SalaryDetail,
    (salaryDetailLists) => salaryDetailLists.salaryId,
  )
  salaryDetailLists: SalaryDetail[];

  // @OneToMany(() => CheckInOut, (cInOut) => cInOut.salary)
  // cInOut: CheckInOut;

  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
  @DeleteDateColumn()
  deletedAt: Date;
}

// import {
//   Column,
//   CreateDateColumn,
//   DeleteDateColumn,
//   Entity,
//   OneToMany,
//   PrimaryGeneratedColumn,
//   UpdateDateColumn,
// } from 'typeorm';
// import { SalaryDetail } from './salary-detail';

// @Entity()
// export class Salary {
//   @PrimaryGeneratedColumn()
//   id: number;
//   @Column()
//   date_start: Date;
//   @Column()
//   date_end: Date;
//   @Column()
//   date_salary: Date;
//   @Column({ type: 'float' })
//   total: number;

//   @OneToMany(
//     () => SalaryDetail,
//     (salaryDetailList) => salaryDetailList.salaryId,
//   )
//   salaryDetailLists: SalaryDetail[];

//   @CreateDateColumn()
//   createdAt: Date;
//   @UpdateDateColumn()
//   updatedAt: Date;
//   @DeleteDateColumn()
//   deletedAt: Date;
// }
