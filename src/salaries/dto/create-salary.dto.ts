import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
export class CreateSalaryDetailDto {
  @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  emp_id: number;
  // @IsNotEmpty()
  // whours: number;
  // @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  // empRate: number;
  // @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  // total: number;

  // emp_name: string;
  // @IsNotEmpty()
  // emp_rate: number;
  @IsNotEmpty()
  emp_whours: number;
  // @IsNotEmpty()
  // emp_total: number;
}
export class CreateSalaryDto {
  @IsNotEmpty()
  date_start: Date;

  @IsNotEmpty()
  date_end: Date;

  @IsNotEmpty()
  date_salary: Date;

  @Type(() => CreateSalaryDetailDto)
  @ValidateNested({ each: true })
  salaryDetailLists: CreateSalaryDetailDto[];

  cionum: number[];

  // @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  // total: number;
}
