import { PartialType } from '@nestjs/mapped-types';
import { CreateMenuQueueDto } from './create-menu_queue.dto';

export class UpdateMenuQueueDto extends PartialType(CreateMenuQueueDto) {}
