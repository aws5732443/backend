import { Material } from './../materials/entities/material.entity';
import { BillMaterialDetail } from './entities/bill-detail';
import { BillMaterial } from './entities/bill_material.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBillMaterialDto } from './dto/create-bill_material.dto';
import { UpdateBillMaterialDto } from './dto/update-bill_material.dto';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class BillMaterialsService {
  constructor(
    @InjectRepository(BillMaterial)
    private billRepository: Repository<BillMaterial>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(BillMaterialDetail)
    private billItemsRepository: Repository<BillMaterialDetail>,
  ) {}

  async create(createBillMaterialDto: CreateBillMaterialDto) {
    // console.log(createBillMaterialDto.employeeId);
    const employee = await this.employeeRepository.findOneBy({
      id: createBillMaterialDto.employeeId,
    });
    // console.log(employee);
    const bill: BillMaterial = new BillMaterial();
    bill.employee = employee;
    bill.shop_name = createBillMaterialDto.shop_name;
    bill.date = createBillMaterialDto.date;
    bill.total = 0;
    bill.buy = createBillMaterialDto.buy;
    bill.change = createBillMaterialDto.change;
    // // console.log(bill);
    await this.billRepository.save(bill);
    // for (const bd of createBillMaterialDto.billItems) {
    //   const billItem = new BillMaterialDetail();
    //   billItem.quantity = bd.quantity;
    //   billItem.material = await this.materialRepository.findOneBy({
    //     id: bd.materialId,
    //   });
    //   billItem.name = billItem.material.name;
    //   billItem.price = bd.price;
    //   billItem.total = billItem.price * billItem.quantity;
    //   billItem.bill = bill;
    //   await this.billItemsRepository.save(billItem);
    //   bill.total += billItem.total;
    // }
    // // console.log(bill);
    // await this.billRepository.save(bill);
    return this.billRepository.findOne({
      where: { id: bill.id },
      relations: ['billItems'],
    });
  }

  findAll() {
    return this.billRepository.find({
      relations: ['billItems.material', 'employee'],
    });
  }

  async findOne(id: number) {
    const bill = await this.billRepository.findOne({
      where: { id: id },
      relations: ['billItems.material', 'employee'],
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return bill;
  }

  async update(id: number, updateBillMaterialDto: UpdateBillMaterialDto) {
    const bill = await this.billRepository.findOne({
      where: { id: id },
      relations: ['billItems.material'],
    });
    if (!bill) {
      throw new NotFoundException();
    }
    if (updateBillMaterialDto.employeeId != undefined) {
      bill.employee = await this.employeeRepository.findOneBy({
        id: updateBillMaterialDto.employeeId,
      });
      // receipt.employee = rec.employee;
    }
    // bill.total = 0;
    bill.total = updateBillMaterialDto.total;
    if (updateBillMaterialDto.billItems != undefined) {
      for (const ubd of updateBillMaterialDto.billItems) {
        const materialf = await this.materialRepository.findOneBy({
          id: ubd.materialId,
        });
        const bd = await this.billItemsRepository.findOneBy({
          name: materialf.name,
          bill: { id: bill.id },
        });
        if (bd) {
          // console.log(bd);
          bill.total -= bd.total;
          materialf.quantity -= bd.quantity;
          bd.quantity = ubd.quantity;
          bd.price = ubd.price;
          bd.total = ubd.price * ubd.quantity;
          bd.bill = bill;
          this.billItemsRepository.save(bd);
          materialf.quantity += bd.quantity;
          this.materialRepository.update(materialf.id, materialf);
          bill.total += bd.total;
        } else {
          const billItem = new BillMaterialDetail();
          billItem.quantity = ubd.quantity;
          billItem.material = materialf;
          billItem.name = billItem.material.name;
          billItem.price = ubd.price;
          billItem.total = billItem.price * billItem.quantity;
          billItem.bill = bill;
          this.billItemsRepository.save(billItem);
          materialf.quantity += billItem.quantity;
          this.materialRepository.update(materialf.id, materialf);
          bill.total += billItem.total;
        }
      }
    }
    bill.shop_name = updateBillMaterialDto.shop_name;
    bill.date = updateBillMaterialDto.date;
    bill.buy = updateBillMaterialDto.buy;
    // for (const bdd of bill.billItems) {
    //   bill.total += bdd.total;
    // }
    if (bill.total <= 0) {
      bill.change = 0;
    } else {
      bill.change = bill.buy - bill.total;
      // bill.change = updateBillMaterialDto.change || bill.buy - bill.total;
    }

    // const billN = { ...bill, ...updateBillMaterialDto };
    // await this.billRepository.save({ ...bill, ...updateBillMaterialDto });
    // // console.log(bill);
    await this.billRepository.save(bill);
    return this.billRepository.findOne({
      where: { id: bill.id },
      relations: ['billItems.material'],
    });
  }

  // async update(id: number, updateBillMaterialDto: UpdateBillMaterialDto) {
  //   const billn = await this.billRepository.findOne({
  //     where: { id: id },
  //     relations: ['billItems.material'],
  //   });
  //   if (!billn) {
  //     throw new NotFoundException();
  //   }
  //   // const items = billn.billItems;
  //   if (updateBillMaterialDto.employeeId != undefined) {
  //     billn.employee = await this.employeeRepository.findOneBy({
  //       id: updateBillMaterialDto.employeeId,
  //     });
  //   }
  //   billn.shop_name = updateBillMaterialDto.shop_name;
  //   billn.buy = updateBillMaterialDto.buy;
  //   await this.billRepository.save(billn);
  //   // console.log(updateBillMaterialDto);
  //   // console.log(updateBillMaterialDto.billItems);
  //   billn.total = 0;
  //   if (updateBillMaterialDto.billItems != undefined) {
  //     for (const ubd of updateBillMaterialDto.billItems) {
  //       const materialf = await this.materialRepository.findOneBy({
  //         id: ubd.materialId,
  //       });
  //       const bd = await this.billItemsRepository.findOneBy({
  //         name: materialf.name,
  //         bill: { id: billn.id },
  //       });
  //       if (bd) {
  //         // console.log(bd);
  //         // billn.total -= bd.total;
  //         bd.quantity = ubd.quantity;
  //         bd.price = ubd.price;
  //         bd.total = ubd.price * ubd.quantity;
  //         bd.bill = billn;
  //         // console.log(bd);
  //         await this.billItemsRepository.save(bd);
  //         // billn.total += bd.total;
  //       } else {
  //         const billItem = new BillMaterialDetail();
  //         billItem.bill = billn;
  //         billItem.quantity = ubd.quantity;
  //         billItem.material = materialf;
  //         billItem.name = billItem.material.name;
  //         billItem.price = ubd.price;
  //         billItem.total = billItem.price * billItem.quantity;

  //         // const billItem = await this.billItemsRepository.save({
  //         //   material: materialf,
  //         //   name: materialf.name,
  //         //   quantity: ubd.quantity,
  //         //   price: ubd.price,
  //         //   total: ubd.quantity * ubd.price,
  //         //   bill: billn,
  //         // });
  //         // // console.log('-----');
  //         // console.log(billItem);
  //         await this.billItemsRepository.save(billItem);
  //         // items.push(billItem);
  //         // billn.total += billItem.total;
  //       }
  //       // await this.billRepository.save(bill);
  //     }
  //     // billn.billItems = items;
  //     // await this.billRepository.save({ ...billn, ...items });
  //   }
  //   for (const bdd of billn.billItems) {
  //     billn.total += bdd.total;
  //   }
  //   if (billn.total <= 0) {
  //     billn.change = 0;
  //   } else {
  //     billn.change = billn.buy - billn.total;
  //     // bill.change = updateBillMaterialDto.change || bill.buy - bill.total;
  //   }

  //   // // console.log(items);
  //   // // console.log({ ...billn, ...items });
  //   await this.billRepository.save(billn);
  //   return this.billRepository.findOne({
  //     where: { id: billn.id },
  //     relations: ['billItems'],
  //   });
  // }

  async remove(id: number) {
    const bill = await this.billRepository.findOne({
      where: { id: id },
      relations: ['billItems.material'],
    });
    if (!bill) {
      throw new NotFoundException();
    }
    for (const bd of bill.billItems) {
      // // console.log(bd);
      const materialf = await this.materialRepository.findOneBy({
        id: bd.material.id,
      });
      await this.billItemsRepository.softRemove(bd);
      materialf.quantity -= bd.quantity;
      this.materialRepository.update(materialf.id, materialf);
    }
    // // console.log(bill);
    return this.billRepository.softRemove(bill);
  }

  async removeItem(id: number) {
    const billItem = await this.billItemsRepository.findOne({
      where: { id: id },
      relations: ['bill', 'material'],
    });
    const bill = await this.billRepository.findOne({
      where: { id: billItem.bill.id },
      relations: ['billItems'],
    });
    if (!billItem) {
      throw new NotFoundException();
    }
    if (!bill) {
      throw new NotFoundException();
    }
    // bill.total = 0;
    // for (const bdd of bill.billItems) {
    //   bill.total += bdd.total;
    // }
    bill.total -= billItem.total;
    const materialf = await this.materialRepository.findOneBy({
      id: billItem.material.id,
    });
    materialf.quantity -= billItem.quantity;
    this.materialRepository.update(materialf.id, materialf);
    bill.change = bill.buy - bill.total;
    this.billRepository.save(bill);
    return this.billItemsRepository.softRemove(billItem);
  }
}
