import { PartialType } from '@nestjs/mapped-types';
import { CreateBillMaterialDto } from './create-bill_material.dto';

export class UpdateBillMaterialDto extends PartialType(CreateBillMaterialDto) {}
