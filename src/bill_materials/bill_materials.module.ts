import { BillMaterialDetail } from './entities/bill-detail';
import { BillMaterial } from './entities/bill_material.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { BillMaterialsService } from './bill_materials.service';
import { BillMaterialsController } from './bill_materials.controller';
import { Material } from 'src/materials/entities/material.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BillMaterial,
      BillMaterialDetail,
      Material,
      Employee,
    ]),
  ],
  controllers: [BillMaterialsController],
  providers: [BillMaterialsService],
})
export class BillMaterialsModule {}
