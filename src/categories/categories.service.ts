import { Category } from './entities/category.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    return this.categoriesRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categoriesRepository.find();
  }

  async findOne(id: number) {
    const category = await this.categoriesRepository.findOne({
      where: { id: id },
    });
    if (!category) {
      throw new NotFoundException();
    }
    return category;
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const category = await this.categoriesRepository.findOneBy({ id: id });
    if (!category) {
      throw new NotFoundException();
    }
    return this.categoriesRepository.save({
      ...category,
      ...updateCategoryDto,
    });
  }
  async remove(id: number) {
    const category = await this.categoriesRepository.findOneBy({ id: id });
    if (!category) {
      throw new NotFoundException();
    }
    return this.categoriesRepository.softRemove(category);
  }
}
