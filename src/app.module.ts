import { CheckMaterial } from './check_materials/entities/check_material.entity';
import { CheckMatDetail } from './check_materials/entities/chmat_detail';
import { Category } from './categories/entities/category.entity';
import { MenuQueue } from 'src/menu_queues/entities/menu_queue.entity';
import { BillMaterialDetail } from './bill_materials/entities/bill-detail';
import { BillMaterial } from './bill_materials/entities/bill_material.entity';
import { Material } from './materials/entities/material.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm/data-source/DataSource';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MaterialsModule } from './materials/materials.module';
import { BillMaterialsModule } from './bill_materials/bill_materials.module';
import { SalariesModule } from './salaries/salaries.module';
import { Salary } from './salaries/entities/salary.entity';
import { TablesModule } from './tables/tables.module';
import { Table } from './tables/entities/table.entity';
import { MenusModule } from './menus/menus.module';
import { Menu } from './menus/entities/menu.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { MenuQueuesModule } from './menu_queues/menu_queues.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { CategoriesModule } from './categories/categories.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { ReceiptDetail } from './receipts/entities/receipt-detail';

import { SalaryDetail } from './salaries/entities/salary-detail';
import { AuthModule } from './auth/auth.module';
import { CheckinoutModule } from './checkinout/checkinout.module';
import { Checkinout } from './checkinout/entities/checkinout.entity';
import { ReportsModule } from './reports/reports.module';
import { CheckMaterialsModule } from './check_materials/check_materials.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      // type: 'sqlite',
      // database: 'db.sqlite',
      type: 'mysql',
      // host: 'db4free.net',
      // port: 3306,
      // username: 'restuarant_g14m',
      // password: 'restuarant_g14m',
      // database: 'restuarant_g14',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'restuarant_g14',
      entities: [
        Material,
        BillMaterial,
        BillMaterialDetail,
        Salary,
        SalaryDetail,
        Table,
        Menu,
        Employee,
        MenuQueue,
        Category,
        Receipt,
        ReceiptDetail,
        Checkinout,
        CheckMatDetail,
        CheckMaterial,
      ],
      synchronize: true,
    }),
    MaterialsModule,
    BillMaterialsModule,
    SalariesModule,
    TablesModule,
    MenusModule,
    EmployeesModule,
    MenuQueuesModule,
    ReceiptsModule,
    CategoriesModule,
    AuthModule,
    CheckinoutModule,
    ReportsModule,
    CheckMaterialsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
