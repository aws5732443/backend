import {
  // Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { CheckMatDetail } from './chmat_detail';

@Entity()
export class CheckMaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  date: Date;

  @ManyToOne(() => Employee, (employee) => employee.checkMaterials)
  employee: Employee;
  @OneToMany(() => CheckMatDetail, (checkItem) => checkItem.checkmat)
  checkItems: CheckMatDetail[];

  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
