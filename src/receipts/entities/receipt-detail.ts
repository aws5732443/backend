import { Menu } from '../../menus/entities/menu.entity';
import { Receipt } from './receipt.entity';
import {
  Entity,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Column,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class ReceiptDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @Column()
  quantity: number;
  @Column({ type: 'float' })
  price: number;
  @Column({ type: 'float' })
  total: number;
  @ManyToOne(() => Menu, (menu) => menu.receiptDetail)
  menu: Menu;
  @ManyToOne(() => Receipt, (receipt) => receipt.receiptDetail)
  receipt: Receipt;
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
