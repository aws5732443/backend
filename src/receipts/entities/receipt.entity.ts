import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Generated,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { MenuQueue } from 'src/menu_queues/entities/menu_queue.entity';
import { ReceiptDetail } from './receipt-detail';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;
  @Generated('uuid')
  @Column({ type: 'uuid', unique: true })
  uuidI: string;
  @Column()
  tablenum: number;
  //@Column()
  @CreateDateColumn()
  date: Date;
  @Column({ type: 'float' })
  subtotal: number;
  @Column({ type: 'float' })
  discount: number;
  @Column({ type: 'float' })
  total: number;
  @Column({ type: 'float' })
  received: number;
  @Column({ type: 'float' })
  change: number;
  @Column()
  status: string;
  @Column()
  payment: string;
  // @CreateDateColumn()
  // createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
  @OneToMany(() => MenuQueue, (menuQueues) => menuQueues.receipt)
  menuQueues: MenuQueue[];
  @ManyToOne(() => Employee, (employee) => employee.receipt)
  employee: Employee;
  @ManyToOne(() => Table, (table) => table.receipt)
  table: Table;

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.receipt)
  receiptDetail: ReceiptDetail[];
}
