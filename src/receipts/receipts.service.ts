import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Receipt } from './entities/receipt.entity';
import { ReceiptDetail } from './entities/receipt-detail';
import { Menu } from 'src/menus/entities/menu.entity';
import { v4 as uuidv4 } from 'uuid';
import { MenuQueue } from 'src/menu_queues/entities/menu_queue.entity';
@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptRepository: Repository<Receipt>,
    @InjectRepository(Table)
    private TableRepository: Repository<Table>,
    @InjectRepository(Employee)
    private EmployeeRepository: Repository<Employee>,
    @InjectRepository(ReceiptDetail)
    private ReceiptDetailRepository: Repository<ReceiptDetail>,
    @InjectRepository(Menu)
    private MenuRepository: Repository<Menu>,
    @InjectRepository(MenuQueue)
    private menuQRepository: Repository<MenuQueue>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    // const Employee = await this.EmployeeRepository.findOneBy({
    //   id: createReceiptDto.empid,
    // });
    const table = await this.TableRepository.findOneBy({
      id: createReceiptDto.tableid,
    });
    // console.log(createReceiptDto);
    // console.log(createReceiptDto.tableid);
    const receipt: Receipt = new Receipt();
    // receipt.employee = Employee;
    receipt.table = table;
    receipt.tablenum = table.num;
    receipt.subtotal = 0;
    receipt.discount = 0;
    receipt.total = 0;
    receipt.received = 0;
    receipt.change = 0;
    receipt.status = 'รอชำระเงิน';
    receipt.payment = '-';
    receipt.uuidI = uuidv4();
    // await this.receiptRepository.save(receipt);
    // console.log(receipt);
    // // console.log(createReceiptDto);
    // return this.receiptRepository.save(createReceiptDto);
    return this.receiptRepository.save(receipt);
  }

  findAll() {
    return this.receiptRepository.find({
      relations: ['employee', 'table', 'receiptDetail.menu'],
    });
  }

  findOne(id: number) {
    return this.receiptRepository.findOne({
      where: { id: id },
      relations: ['employee', 'table', 'receiptDetail.menu'],
    });
  }
  findOneByUUid(uuidI: string) {
    return this.receiptRepository.findOne({
      where: { uuidI: uuidI },
      relations: ['employee', 'table', 'receiptDetail.menu'],
    });
  }

  // async update(id: number, updateReceiptDto: UpdateReceiptDto) {
  //   const receipt = await this.receiptRepository.findOne({
  //     where: { id: id },
  //   });

  //   // const editReceipt: Receipt = new Receipt();
  //   // editReceipt.tablenum = updateReceiptDto.tablenum;
  //   // editReceipt.total = updateReceiptDto.total;
  //   // editReceipt.discount = updateReceiptDto.discount;
  //   // editReceipt.subtotal = updateReceiptDto.subtotal;
  //   // editReceipt.received = updateReceiptDto.received;
  //   // editReceipt.change = updateReceiptDto.change;
  //   // editReceipt.payment = updateReceiptDto.payment;
  //   // editReceipt.status = updateReceiptDto.status;

  //   if (updateReceiptDto.empid != undefined) {
  //     const employee = await this.EmployeeRepository.findOneBy({
  //       id: updateReceiptDto.empid,
  //     });
  //     receipt.employee = employee;
  //   }
  //   if (updateReceiptDto.tableid != undefined) {
  //     const table = await this.TableRepository.findOneBy({
  //       id: updateReceiptDto.tableid,
  //     });
  //     receipt.table = table;
  //     receipt.tablenum = table.num;
  //   }

  //   if (updateReceiptDto.receiptDetail != undefined) {
  //     for (const urd of updateReceiptDto.receiptDetail) {
  //       const menu = await this.MenuRepository.findOneBy({
  //         id: urd.menuId,
  //       });
  //       const rcd = await this.ReceiptDetailRepository.findOneBy({
  //         name: menu.name,
  //         receipt: { id: receipt.id },
  //       });
  //       if (rcd) {
  //         rcd.quantity += urd.quantity;
  //         rcd.total += rcd.price * urd.quantity;
  //         await this.ReceiptDetailRepository.save(rcd);
  //         receipt.subtotal += rcd.price * urd.quantity;
  //       } else {
  //         const receiptDetail = new ReceiptDetail();
  //         receiptDetail.quantity = urd.quantity;
  //         receiptDetail.menu = menu;
  //         receiptDetail.name = receiptDetail.menu.name;
  //         receiptDetail.price = receiptDetail.menu.price;
  //         receiptDetail.total = receiptDetail.price * receiptDetail.quantity;
  //         receiptDetail.receipt = receipt;
  //         await this.ReceiptDetailRepository.save(receiptDetail);
  //         receipt.subtotal += receiptDetail.total;
  //       }
  //     }
  //   }

  //   if (updateReceiptDto.discount != undefined) {
  //     receipt.total = receipt.subtotal - updateReceiptDto.discount;
  //   } else {
  //     receipt.total = receipt.subtotal;
  //   }

  //   if (updateReceiptDto.received != undefined) {
  //     receipt.received = updateReceiptDto.received;
  //     receipt.change = updateReceiptDto.received - receipt.total;
  //     receipt.status = updateReceiptDto.status;
  //     receipt.payment = updateReceiptDto.payment;
  //   }

  //   // const updatedReceipt = { ...receipt, ...editReceipt };
  //   // return this.receiptRepository.save(updatedReceipt);
  //   await this.receiptRepository.save(receipt);
  //   return this.receiptRepository.findOne({
  //     where: { id: receipt.id },
  //     relations: ['receiptDetail'],
  //   });
  // }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    const receipt = await this.receiptRepository.findOne({
      where: { id: id },
      relations: ['table'],
    });

    if (updateReceiptDto.empid != undefined) {
      receipt.employee = await this.EmployeeRepository.findOneBy({
        id: updateReceiptDto.empid,
      });
      // receipt.employee = rec.employee;
    }
    if (updateReceiptDto.tableid != undefined) {
      const table0 = await this.TableRepository.findOneBy({
        id: receipt.table.id,
      });
      const table = await this.TableRepository.findOneBy({
        id: updateReceiptDto.tableid,
      });
      if (table0.status == 'ไม่ว่าง' && table.status == 'ว่าง') {
        table0.status = 'กำลังจะว่าง';
        await this.TableRepository.update(table0.id, table0);
        table.status = 'ไม่ว่าง';
        await this.TableRepository.update(table.id, table);

        receipt.table = await this.TableRepository.findOneBy({
          id: updateReceiptDto.tableid,
        });
        receipt.tablenum = receipt.table.id;
      }
      // else {
      //   throw Error();
      // }
      // receipt.table = table;
      // receipt.tablenum = table.num;
    }

    if (updateReceiptDto.receiptDetail != undefined) {
      for (const urd of updateReceiptDto.receiptDetail) {
        const menu = await this.MenuRepository.findOneBy({
          id: urd.menuId,
        });
        const rcd = await this.ReceiptDetailRepository.findOneBy({
          name: menu.name,
          receipt: { id: receipt.id },
        });
        if (rcd) {
          rcd.quantity += urd.quantity;
          rcd.total += rcd.price * urd.quantity;
          await this.ReceiptDetailRepository.save(rcd);
          receipt.subtotal += rcd.price * urd.quantity;
        } else {
          const receiptDetail = new ReceiptDetail();
          receiptDetail.quantity = urd.quantity;
          receiptDetail.menu = menu;
          receiptDetail.name = receiptDetail.menu.name;
          receiptDetail.price = receiptDetail.menu.price;
          receiptDetail.total = receiptDetail.price * receiptDetail.quantity;
          receiptDetail.receipt = receipt;
          await this.ReceiptDetailRepository.save(receiptDetail);
          receipt.subtotal += receiptDetail.total;
        }
      }
    }

    if (updateReceiptDto.discount != undefined) {
      receipt.discount = updateReceiptDto.discount;
      receipt.total = receipt.subtotal - updateReceiptDto.discount;
    } else {
      receipt.total = receipt.subtotal;
    }

    if (updateReceiptDto.received != undefined) {
      receipt.received = updateReceiptDto.received;
      if (updateReceiptDto.received > 0) {
        receipt.change = updateReceiptDto.received - receipt.total;
      }
      receipt.status = updateReceiptDto.status;
      receipt.payment = updateReceiptDto.payment;
    }

    // const updatedReceipt = { ...receipt, ...editReceipt };
    // return this.receiptRepository.save(updatedReceipt);
    await this.receiptRepository.save(receipt);
    return this.receiptRepository.findOne({
      where: { id: receipt.id },
      relations: ['receiptDetail'],
    });
  }

  async remove(id: number) {
    const receipt = await this.receiptRepository.findOne({
      where: { id: id, status: 'รอชำระเงิน' },
      relations: ['receiptDetail.menu', 'table'],
    });
    // console.log(receipt);
    if (!receipt) {
      throw new NotFoundException();
    }
    let mnQ = await this.menuQRepository.findOneBy({
      receipt: { id: receipt.id },
    });
    // console.log(mnQ);
    for (const rec of receipt.receiptDetail) {
      await this.ReceiptDetailRepository.softRemove(rec);
    }
    while (mnQ) {
      if (mnQ) {
        mnQ.status = 'ยกเลิกรายการ';
        await this.menuQRepository.save(mnQ);
        this.menuQRepository.softRemove(mnQ);
      }
      mnQ = await this.menuQRepository.findOneBy({
        receipt: { id: receipt.id },
      });
      // console.log(mnQ);
    }
    const table = await this.TableRepository.findOneBy({
      id: receipt.table.id,
    });
    table.status = 'กำลังจะว่าง';
    // console.log(table);
    await this.TableRepository.update(table.id, table);
    return this.receiptRepository.softRemove(receipt);
  }

  async removeItem(id: number) {
    const receiptDetail = await this.ReceiptDetailRepository.findOne({
      where: { id: id },
      relations: ['receipt', 'menu'],
    });
    const receipt = await this.receiptRepository.findOne({
      where: { id: receiptDetail.receipt.id, status: 'รอชำระเงิน' },
      relations: ['receiptDetail'],
    });
    if (!receiptDetail) {
      throw new NotFoundException();
    }
    if (!receipt) {
      throw new NotFoundException();
    }
    receipt.subtotal -= receiptDetail.total;
    let mnQ = await this.menuQRepository.findOne({
      where: {
        receipt: { id: receipt.id },
        menu: { id: receiptDetail.menu.id },
      },
      relations: ['receipt', 'menu'],
    });
    if (!mnQ) {
      throw new NotFoundException();
    }
    while (mnQ) {
      if (mnQ) {
        mnQ.status = 'ยกเลิกรายการ';
        await this.menuQRepository.save(mnQ);
        this.menuQRepository.softRemove(mnQ);
      }
      mnQ = mnQ = await this.menuQRepository.findOne({
        where: {
          receipt: { id: receipt.id },
          menu: { id: receiptDetail.menu.id },
        },
        relations: ['receipt', 'menu'],
      });
      // console.log(mnQ);
    }
    receipt.total = receipt.subtotal - receipt.discount;
    this.receiptRepository.save(receipt);
    return this.ReceiptDetailRepository.softRemove(receiptDetail);
  }
}
