import { IsNotEmpty } from 'class-validator';
export class CreateReceiptDto {
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  // tablenum: number;
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  // date: Date;
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  // subtotal: number;
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  discount: number;
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  // total: number;
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  received: number;
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  // change: number;
  // @Length(3, 12)
  @IsNotEmpty()
  status: string;
  @IsNotEmpty()
  payment: string;
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  empid: number;
  // @IsPositive()
  // @IsInt()
  // @IsNotEmpty()
  tableid: number;

  receiptDetail: CreateReceiptDetailDto[];
}

class CreateReceiptDetailDto {
  menuId: number;

  quantity: number;
}
