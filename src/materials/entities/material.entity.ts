import { BillMaterialDetail } from './../../bill_materials/entities/bill-detail';
import { CheckMatDetail } from './../../check_materials/entities/chmat_detail';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  name: string;
  @Column()
  min_quantity: number;
  @Column()
  quantity: number;
  @Column()
  unit: string;
  @Column({ type: 'float' })
  unit_price: number;

  @OneToMany(() => BillMaterialDetail, (billItem) => billItem.material)
  billItems: BillMaterialDetail[];

  @OneToMany(() => CheckMatDetail, (checkItem) => checkItem.material)
  checkItems: CheckMatDetail[];

  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
  @DeleteDateColumn()
  deletedAt: Date;
}
