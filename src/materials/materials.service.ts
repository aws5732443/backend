import { Material } from './entities/material.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  create(createMaterialDto: CreateMaterialDto) {
    return this.materialsRepository.save(createMaterialDto);
  }

  findAll() {
    return this.materialsRepository.find();
  }

  async findOne(id: number) {
    const material = await this.materialsRepository.findOne({
      where: { id: id },
    });
    if (!material) {
      throw new NotFoundException();
    }
    return material;
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    // console.log(updateMaterialDto);
    const material = await this.materialsRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.materialsRepository.save({ ...material, ...updateMaterialDto });
  }

  async remove(id: number) {
    const material = await this.materialsRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.materialsRepository.softRemove(material);
  }
}
